import UIKit
import GamedaySDK

@objc(GamedayMain) class GamedayMain : CDVPlugin {
  @objc(startGameday:) // Declare your function name.
  func startGameday(command: CDVInvokedUrlCommand) {
    var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "The Plugin Failed");
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "The plugin succeeded");

    GamedayManager.shared.start()
    self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
  }
  @objc(stopGameday:) // Declare your function name.
  func stopGameday(command: CDVInvokedUrlCommand) {
    var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "The Plugin Failed");
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "The plugin succeeded");

    GamedayManager.shared.stop()
    self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
  }
}