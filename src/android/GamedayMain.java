package com.gameday.cordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONArray;
import java.io.IOException; 
import java.util.logging.Level; 
import java.util.logging.Logger; 
import java.util.logging.*; 
import android.content.Context;

import com.gbe.gameday.gbegamedaysdk.controllers.GamedayController;

public class GamedayMain extends CordovaPlugin {

    private final static Logger LOGGER =  Logger.getLogger(Logger.GLOBAL_LOGGER_NAME); 
    
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if (action.equals("startGameday")) {
            Context context = this.cordova.getActivity().getApplicationContext();
            GamedayController gameday = new GamedayController(context);
            LOGGER.log(Level.INFO, "Starting Gameday...");
            gameday.start();
            return true;
        } else if(action.equals("stopGameday")) {
            Context context = this.cordova.getActivity().getApplicationContext();
            GamedayController gameday = new GamedayController(context);
            LOGGER.log(Level.INFO, "Stoping Gameday...");
            gameday.pause();
            gameday.shutdown();
            return true;
        } else {
            LOGGER.log(Level.INFO, "Unrecognized command!");
        }
        // Unrecognized command
        return false;
    }
}
