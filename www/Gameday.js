const exec = require('cordova/exec');

let Gameday = {
    startGameday: (input, successCallback, errorCallback) => {
        exec(successCallback, errorCallback, "GamedayMain", "startGameday");
    },
    stopGameday: (input, successCallback, errorCallback) => {
        exec(successCallback, errorCallback, "GamedayMain", "stopGameday");
    }
};

module.exports = Gameday